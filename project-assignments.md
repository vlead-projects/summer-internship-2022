# Introduction

All the project assignments related to 2022 Summer Internship at VLEAD are captured here.

# Project List 

Each intern will be working on two projects during the internship period. The first project will entail conversion of an existing IIITH developed flash based Virtual Lab to Javascript. The second project will be of intern's interest from the [list](https://docs.google.com/spreadsheets/d/1tQEiIkeREHGacKdQFMlSsP5CfPZ3Jor9TmdGZAthEYw/edit#gid=1112618416) of Virtual Labs enhancements projects.

# Interns List

| S.No | Name of the Student | Email ID |Github | Phone Number|
| :---: | :---: | :---: |:---: | :---: |
| 1. |  | | | |
| 2. |  | | | |
| 3. |  | | | |


# Project Assignments - Part 1

| S.No | Inter Name | Email address/Phone Number |Github Handle/Gitlab Handle |Lab Name/Hosted Link/Non js repo link|Assigned Experiment names/repo links|
| :---: | :---: | :---: |:---: | :---: | :---: |
| 1. |  | | | | | 

# Internship Agenda

## General Instructions 

### Development Process for Lab Conversion

Step 1 : Choose one exp from the assigned list and send a mail to systems@vlabs.ac.in with your Github handle and associated email id requesting to be added to the repository.

Step 2 : Clone the repository.

Step 3 : Populate the dev branches of the repository with the source code of the experiments and unit test the experiments locally. Do not   delete gh-pages branch. This is required for automatically deploying the experiment along with UI on GitHub pages for testing the complete experiment. Also do not delete .github directory and the LICENSE file.

Step 4 : Merge the fully tested dev branch to testing branch. This will automatically deploy the experiment along with UI on GitHub pages for  testing the complete experiment. You will receive an email about the success/failure of the deployment on the email id associated with the github handle provided in Step 1.

Step 5 : Set up a demo with the Virtual Labs Team and merge the code with main branch only after getting approval from them.

### Realization in terms of Milestones

Follow agile software development and use scrum methodology to incrementally working software.  Every project has a product backlog.  In each sprint, items from the product backlog are picked to be realized in a milestone.  Each item from a product backlog translates to one or more tasks and each task is tracked as an issue on github.  A milestone is a collection of issues. It is upto the mentor and the team to choose either one or two week sprints.  Each release is working software and a release determines the achievement of a milestone. A project is realized as a series of  milestones.  This planning will be part of the master repo of respective project repos.

### Communication

Every discussion about the project will be through issues.  Mails are not used for discussion.  Any informal communication will be through Slack - 2022-summer-interns channel.

### Things done at the weekly meeting

1. A presentation of the previous week's plan, accomplished tasks and backlogs. 
2. Discussion of code, documents and a demonstration.
2. Review of pull/merge request if any.
3. Task planning for the next week.  The students will be responsible for taking the meeting notes.  The meeting notes may follow the structure as shown below. 
    - Agenda
    - Discussion Points
    - Action Items

### Work logs 

The work logs of each intern will need to be pushed to the directory shared in the [On-boarding document](https://gitlab.com/vlead-projects/summer-internship-2022/-/blob/main/on-boarding.md).

### Team Meetings

Team meetings will be held every Tuesday/Thursday during the morning hours with a slotted time of 30 min per intern.

### Slack Channel

Communication will be through [Slack](https://vleadworkspace.slack.com) during the internship period. If you need any guidance regarding anything, just post it on [2022-summer-interns](https://vleadworkspace.slack.com/archives/C012G7UKTC3) channel. To get access to this channel send out a mail to *pavan@vlabs.ac.in*.  Please be available at all times on [2022-summer-interns](https://vleadworkspace.slack.com/archives/C012G7UKTC3) channel on [Slack](https://vleadworkspace.slack.com/messages).





