# Introduction

  This document holds the timeline for my work during 2022 Summer Internship at VLEAD, IIIT Hyderabad.

# Work Logs

## Week 1 [2022-05-16 Mon] till [2022-05-20 Fri]

## Daily Logs

- Day1 [2022-05-16 Mon]: Introductory Meet assigned projects to work on , and discussed about the internship plan as we go through.
- Day2 [2022-05-17 Tue] : Going through the given documentation , checking the progress of already completed experiments, progress of the previous meet on the experiment , going through the code base , understanding the code base.
- Day3 [2022-05-18 Wed] : Going through the codebase , understanding the code and choosing counters experiment to get started.
- Day4 [2022-05-19 Thu] : Going through theory of basic and ring counters, building the practice subsection for basic counters
- Day5 [2022-05-20 Fri] : Practice subsection of basic counter completed

## Week 2 [2022-05-23 Mon] till [2022-05-27 Fri]

## Daily Logs

- Day1 [2022-05-23 Mon] : Building practice subsection of ring counter, fixing bugs of practice subsection of basic counter
- Day2 [2022-05-24 Tue] : Practice subsection of ring counter completed
- Day3 [2022-05-25 Wed] : Going through GSAP library to understand how to make animations, learning how to make svg
- Day4 [2022-05-26 Thu] : Building animation for basic counter
- Day5 [2022-05-27 Fri] : Fixing bugs in animation and providing final touches to animation of basic counter, showing the progress to mentors,removed alert boxes and showed descriptive errors, removed bugs, pushing code to github

## Week 3 [2022-05-30 Mon] till [2022-06-03 Fri]

## Daily Logs

+ Day1 [2022-05-30 Mon]: Finishing touches to the experiment, corrected experiment descriptor, edited procedure.md and created highlight features for the incorrectly connected components to the user.
+ Day2 [2022-05-31 Tue] : added and changed theory, procedure and instructions for the experiment, made suggested changed according to the code review - Code Refactoring
+ Day3 [2022-06-01 Wed] : Created pretest.json and posttest.json and corrected the experiment descriptor.json
+ Day4 [2022-06-02 Thu] : Changed the whole structure of animation part, reduced global variables, pushed the code to testing branch.
+ Day5 [2022-06-03 Fri] : Checked the UI and the files after pushing the changes to testing

## Week 4 [2022-06-06 Mon] till [2022-06-10 Fri]

## Daily Logs

+ Day1 [2022-06-06 Mon]: Started working on second track(Google Analytics) and revised the theory for the next experiment state diagram
+ Day2 [2022-06-07 Tue] : Made the animation for the state diagram experiment, added the state diagram in the observation table, after the simulation user is also able to see the state table
+ Day3 [2022-06-08 Wed] : Refactoring the code for the first experiment, improving UI.
+ Day4 [2022-06-09 Thu] : completed the animation part, started with practice section, edited the practice.html and layout.js. Changed the asynchronous nature of JK flip-flops to synchronous.
+ Day5 [2022-06-10 Fri] : Discussed the implementation of GA4 and GTM and built a simple html page for testing

## Week 5 [2022-06-13 Mon] till [2022-06-17 Fri]

## Daily Logs

+ Day1 [2022-06-13 Mon]: Completed the validation and simulation part of the experiment state diagram,created posttest, pretest for state diagrams experiment along with code refactoring, resolved issues with testing Google analytics on sample html page.
+ Day2 [2022-06-14 Tue]: Code refactoring
+ Day3 [2022-06-15 Wed]: Started demo section for multipliers 
+ Day4 [2022-06-16 Thu]: Completed demo section for multipliters and started demo section of ALU
+ Day5 [2022-06-17 Fri]: Completed demo section of ALU 

## Week 6 [2022-06-20 Mon] till [2022-06-24 Fri]

## Daily Logs

+ Day1 [2022-06-20 Mon]: Completed practice section of multipliers
+ Day2 [2022-06-21 Tue]: Completed practice section of ALU
+ Day3 [2022-06-22 Wed]: Fixed bugs in multipliers demo
+ Day4 [2022-06-23 Thu]: Fixed bugs in ALU practice section
+ Day5 [2022-06-24 Fri]: Fixed bugs in ALU demo section started implementing GA4.

## Week 7 [2022-06-27 Mon] till [2022-07-01 Fri]

## Daily Logs

+ Day1 [2022-06-27 Mon]: Fixed bugs in practice section of ALU, resolved issues regarding permissions
+ Day2 [2022-06-28 Tue]: Code refactoring, got to know about containers being used and triggers/tags
+ Day3 [2022-06-29 Wed]: Code refactoring of counters experiments 
+ Day4 [2022-06-30 Thu]: Fixed bugs in counters experiments
+ Day5 [2022-07-01 Fri]: Code refactoring, upgrading to GA4, testing and previewing.

## Week 8 [2022-07-04 Mon] till [2022-07-08 Fri]

## Daily Logs

+ Day1 [2022-07-04 Mon]: Started working on the demo section of the experiment https://github.com/virtual-labs/exp-transistor-level-inverter-iiith. 
+ Day2 [2022-07-05 Tue]: Finished the demo section of the above experiment.
+ Day3 [2022-07-06 Wed]: Refactor all the demos of the 2 DLD experiment.
+ Day4 [2022-07-07 Thu]: Refactor the demos of next 2 DLD experiment.
+ Day5 [2022-07-08 Fri]: Custom dimensions in GA4.

## Week 9 [2022-07-11 Mon] till [2022-07-15 Fri]

## Daily Logs

+ Day1 [2022-07-11 Mon]: Completed the demo section of the experiment `exp-transistor-level-nand-iiith`, there were 2 subtasks for the demo in this experiment that are nand and nor logic. For these 2 parts, the wires will intersect at some places as a result we have used wires of 2 different colours to distinguish between the wires.
+ Day2 [2022-07-12 Tue]: Completed the demo section of the experiment `exp-pass-transistor-logic-iiith`. There were 2 subtasks for the demo in this experiment that are pass transistor and multiplexer. 
+ Day3 [2022-07-13 Wed]: Completed the demo section of the experiment
  `exp-transistor-level-xor-iiith` . There were 2 subtasks of XOR and XNOR logic.
+ Day4 [2022-07-14 Thu]: Today's work update: Completed the demo section of `exp-d-latch-and-d-flip-flop-iiith` . There were 2 subtasks: Latch and FlipFlop.
+ Day5 [2022-07-15 Fri]: Refactored the above demo sections.

## Week 10 [2022-07-18 Mon] till [2022-07-22 Fri]

## Daily Logs

+ Day1 [2022-07-18 Mon]: Made changes to the practice section of the first VLSI experiment and implemented the different colors of wires.
+ Day2 [2022-07-19 Tue]: Resolved the issues in the first experiment and made similar changes in the following 2 experiments i.e https://github.com/virtual-labs/exp-transistor-level-xor-iiith and https://github.com/virtual-labs/exp-transistor-level-nand-iiith.
+ Day3 [2022-07-20 Wed]: Made demo and practice section of chain of inverters
+ Day4 [2022-07-21 Thu]: Updated the practice section of remaining two VLSI experiment updated instruction box and experiment-descriptor.json.
+ Day5 [2022-07-22 Fri]: Made mobile version of the VLSI experiments.

# Week Progress

| S.No | Overall Experiment Status | Previous Week Milestone | Next Week Milestone | Issues|
| :---: | :---: | :---: |:---: |:---: |
| 1. |Started with experiment 7 |Reviewed and understood the existing codebase | Complete practice subsection of ring counter and demo sections for basic counter | Understanding the libraries to be used, understanding theory behind the experiments|
| 2. |Practice and demo subsection completed for both basic and ring counter (Experiment 7 completed) |Finishing practice subsection of basic counter|Finishing practice subsection of next experiment|Displaying descriptive error messages in experiment, highlighting components with incorrect connections. According to suggestions have to make changes so that the user have a smooth and enriched experience. |
| 3. |Refactored Experiment 7 |Experiment 7 completed |1. Finishing demo section of the next experiment | Code refactored with all the suggested changes |
| 4. |Demo section of Experiment 10. Testing GA4 on sample html page for Second track |Refactored Experiment 7 |1. Finish Experiment 10 and the next experiment, look at GA3 configuration |Make a button on sample html page for capturing clicks, finished the demo section as well as simulation part of the practice section of the state diagram experiment |
| 5. |Multiplier Experiment |Experiment 7 completed |1. Finishing demo section of the next experiment| None |
| 6. |ALU |Experiment 7 completed |1. Finishing demo section of the next experiment | None |
| 7. |Code refactor |Experiment 7 completed |1. Finishing demo section of the next experiment | None |
| 8. |Animation of 4 VLSI experiments |Experiment 7 completed |1. Finishing demo section of the next experiment | None |
| 9. |Animation of 2 VLSI experiments and improved 2 more experiments |Experiment 7 completed |1. Finishing demo section of the next experiment | None |
| 10. | Improved remaining experiments |Experiment 7 completed | None | None |




 











