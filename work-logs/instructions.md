# Introduction

  This document captures the instructions to create work logs for 2022 summer interns.

# Instructions

  Please find the instructions to create work logs.

  1. Create a copy of [template.md](./template.md) file [here](../work-logs)

  2. Rename [template.md](./template.md) with your name *(Example: intern-name.md)*

  3. Update your work logs link [here](./index.md)

  4. The created file has a *work logs* section, where you need to update your work done during the internship period.
 
