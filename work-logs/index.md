# Introduction

  Work logs of all the interns are collated here.

| S.No | Intern Name | Work Logs Link |
| :---: | :---: | :---: |
| 1. |  Karmanjyot Singh | [link](https://gitlab.com/Karmanjyot1232/vleads-intern-worklogs/-/blob/main/karman.md) |
| 2. | Aditya Malhotra | [link](./Aditya.md) |
| 3. |  Gautam Ghai | [link](./Gautam.md)  |
| 4. | Shreyash Jain | [link](./Shreyash.md)|
| 5. | Shashank Ramachandran | [link](./Shashank.md)|
| 6. | Max Greenspan | [link](./Max.md)|
 













