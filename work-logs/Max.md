# Introduction

  This document holds the time sheets for my work during 2022 Summer Internship at VLEAD, IIIT Hyderabad.
  
# Work Logs

## Week 1 [2022-05-30 Mon] till [2022-06-03 Fri]

## Daily Logs

- Day1 [2022-05-30 Mon]: Meeting with Internship and assigned tasks of experiment critical review. Continued learning JavaScript. Practiced using MD files.

- Day2 [2022-05-31 Tue] : Continued learning JavaScript. Practiced using Node.JS. Completed Critical Review of Bubble Sort Virtual Lab.

- Day3 [2022-06-01 Wed] : Continued learning JavaScript.

- Day4 [2022-06-02 Thu] : Continued learning JavaScript. Began working on Celtics Website Project.

- Day5 [2022-06-03 Fri] : Continued learning JavaScript. Completed Critical Review of Stacks and Queues Virtual Lab.

## Week 2 [2022-06-06 Mon] till [2022-06-10 Fri]

## Daily Logs

- Day 1 [2022-06-06 Mon]: Status update meeting, reviewed bugs/critiques for Bubble Sort and Stacks and Queues Labs. Continued Learning JavaScript. Created repo for Lab review files.

- Day 2 [2022-06-07 Tue]: Continued learning JavaScript. Created issue #13 on GitHub Stacks and Queues Lab and pushed changes to branch.

- Day 3 [2022-06-08 Wed]:Continued learning JavaScript. Created issue #14 on GitHub Merge Sort Lab and pushed changes to branch. Looked over other labs for potential changes/improvements.

- Day 4 [2022-06-09 Thr]:Analyzed Heap Sort, BFS, Infix to PostFix, and Tree Traversal labs and found errors/bugs/improvements to make.

- Day 5 [2022-06-10 Fri]: Analyzed Binary Search Tree and Unsorted Array labs and found errors/bugs/improvements to make. Pushed changes for small gramatical errors in both labs.

## Week 3 [2022-06-13 Mon] till [2022-06-17 Fri]

## Daily Logs
 
- Day 1 [2022-06-13 Mon]: Status update meeting, went over all found errors/bugs/fixes. Created Issues and pull request for most labs for the simple errors.

- Day 2 [2022-06-14 Tue]: Categorized issues to address, began working on fixes for DS1 labs.

- Day 3 [2022-06-15 Wed]: Finished uploading pull requests for changes Depth First Search - Analysis - Comparisons of BFS and DFS. Added similar changes to BFS lab. Added minor changes to Unsorted Arrays.

- Day 4 [2022-06-16 Thr]: Finished fixing 2 Infix to Postfix related bugs. Worked on centering error on BFT practice section. 

- Day 5 [2022-06-17 Fri]: Created Critical Reviews static site. Continued working on bugs. Met with Pavan and learned about Hosting Process.

## Week 4 [2022-06-20 Mon] till [2022-06-24 Fri]

## Daily Logs
 
- Day 1 [2022-06-20 Mon]: Continued working on smaller bugs, looked at legend bug.

- Day 2 [2022-06-21 Tue]: Status update. Worked on fixing the UI of the legend.

- Day 3 [2022-06-22 Wed]: Meeting with Shreyash about debugging. Fixed the Legend error.

- Day 4 [2022-06-23 Thr]: Addressed changes with Raj, made followup changes.

- Day 5 [2022-06-24 Fri]:  Continued working on legend and proposed changes.

## Week 5 [2022-06-27 Mon] till [2022-06-31 Fri]

- Day 1 [2022-06-27 Mon]: Status update, addressed and finalized template changes for legend. Began implementation in DFS.

- Day 2 [2022-06-28 Tue]: Finished changes to DFS Demo, Exercise, and Practice, and for Quick-Sort Demo, Exercise, Practice, and Demo: Practice sections.

- Day 3 [2022-06-29 Wed]: Continued working on legend changes for DS1.

- Day 4 [2022-06-30 Thr]: Continued working on legend changes for DS1.

- Day 5 [2022-06-31 Fri]: Finished legend changes for DS1.

## Week 6 [2022-07-04 Mon] till [2022-07-08 Fri]

- Day 1 [2022-07-04 Mon]: Status update, small minor feedback to Legend DS1 changes. 

- Day 2 [2022-07-05 Tue]: Began working on legend changes for DS2.

- Day 3 [2022-07-06 Wed]: Worked on Legend changes for DS2.

- Day 4 [2022-07-07 Thr]: Fixed BST Search and Demo instructions not opening issue.

- Day 5 [2022-07-08 Fri]: Finished implementing legend changes for DS2 except those which did not appear on github (MST and DSP)

## Week 7 [2022-07-11 Mon] till [2022-07-15 Fri]

- Day 1 [2022-07-11 Mon]: Status Update, Finished Legend Changes for DS2 Minimum Spanning Trees and Dijkstra’s Algorithm.

- Day 2 [2022-07-12 Tue]: Changed canvas size for Depth First Search and Breadth First Search experiments.

- Day 3 [2022-07-13 Wed]: Worked on adding functionality to Binary Search Trees Search Practice section.

- Day 4 [2022-07-14 Thr]: Continued working on adding functionality to Binary Search Trees Search Practice section.

- Day 5 [2022-07-15 Fri]: Continued working on adding functionality to Binary Search Trees Search Practice section. Added functionality in response to chosing the incorrect direction, updated the Instructions, and made other similar changes.

## Week 8 [2022-07-18 Mon] till [2022-07-22 Fri]

- Day 1 [2022-07-18 Mon]: Implemented text interaction to wrong direction, changed CSS sizes. Worked on removing verify button and turning next button into a reset button. Met with Pavan to discuss assets folder. Status meeting update, went over and discussed proposed changes to BST-Search feature.

- Day 2 [2022-07-19 Tue]: Continued working on removing verify button and turning next button into a reset button.

- Day 3 [2022-07-20 Wed]: Removed assets folder from UI Legend Changes. Continued work on BST-Search feature.

- Day 4 [2022-07-21 Thr]: Continued work on BST-Search feature.

- Day 5 [2022-07-22 Fri]: Continued work on BST-Search feature.

## Week 9 [2022-07-25 Mon] till [2022-07-29 Fri]

- Day 1 [2022-07-25 Mon]: Status update meeting about BST-Search feature. Began working on state variables.

- Day 2 [2022-07-26 Tue]: Began constructing the Insertion Sort Experiement.

- Day 3 [2022-07-27 Wed]: Continued research and planning.

- Day 4 [2022-07-28 Thr]: Structured the experiment and began writing MD files.

- Day 5 [2022-07-29 Fri]: Continued writing MD Files

## Week 10 [2022-08-01 Mon] till [2022-08-05 Fri]

- Day 1 [2022-08-01 Mon]: Continued work on Insertion Sort.

- Day 2 [2022-08-02 Tue]: Status update meeting, continued work on Insertion sort and json files.

- Day 3 [2022-08-03 Wed]: Continued work on Insertion Sort experiment and experiment-descriptor.json.

- Day 4 [2022-08-04 Thr]: Constructed legend for Demo, Exercise, and Practice files.

- Day 5 [2022-08-05 Fri]: Moved work to testing branch, and began testing code.


# Week Progress

| S.No | Overall Experiment Status | Previous Week Milestone | Next Week Milestone | Issues|
| :---: | :---: | :---: |:---: |:---: |
| 1. | Familiarized with Virtual Labs GitHub code base. | N/A | Implementing these concepts into a project. | Still trying to synthesize all of the concepts of HTML CSS and JS into one cohesive project. |
| 2. | Completed 7 pull request of minor errors in varius DS1 labs. | Gained familiarity with the tools required to work with the labs. | Continue grasping more of the nuances of the code base in order to solve larger, more prominent bugs. | Hard to pinpoint the origin of the errors in the code base, not an issue, rather a work in progress.|
| 3. | Worked on improvements to Infix to Postfix and Tree Traversal Labs | Completed all minor changes to the labs | Fix the legend error that occurs in numerous labs | Still getting more comfortable with debugging and finding the source of an error |
| 4. | Configured proper UI for Legend Changes | Created Static Site and worked on harder bugs | Implementing the Legend Changes in the 40 lab locations where it occurs | Much more comfortable with the code base, but still finding it difficult to make large JS related code changes|
| 5. | Completed Legend Changes for Data Structures 1 experiments | Finalized Legend Concept | Implement Legend Changes for Data Structures 2 experiments | Certain labs have different element layouts, making the UI appear different, requiring additional changes to make legend appear properly|
| 6. | Legend Changes for DS2 essentially completed. | Finished Legend Changes for DS1 | Add a feature / functionality to an aspect of a Data Structures Experiment | Certain Labs not properly labeled in respository, and overlapping files. |
| 7. | Legend Changes for DS2 Completely finished. Canvas changes to DFS and BFS Labs. Began working on implementing BST Search Section functionality. | Legend Changes finished. | Complete BST Search Section changes.| Making proposed changes to BST-Search JS file is difficult without creating additional errors, since many of the essential functions are reused multiple times with callbacks. |
| 8. | Added text interaction to BST Search Feature, css style changes, and removed asset folders from DS1 and DS2 repositories. | Found areas for improvement on BST Search Feature. |Complete restructuring of layout of buttons for functionality, removing verify constraint and adding a reset button. | BST-Search JS file difficult to change and requires changes to state variables.|
| 9. | Outlined and Planned Insertion Sort lab.  | Made more progress on BST-Search Feature. | Complete Insertion Sort pedagogy, finish MD files and structuring of lab. | Deciding the method of teaching Insertion Sort is a challenge, although there are no notable issues at this time. |
| 10. |Completed Draft of Insertion Sort Experiment | Structured entire Insertion Sort Experiment. | Finish json file for testing. | Index.HTML file is missing from build, and needs to be completed before the testing branch can properly display the changes to the Insertion Sort Exp.|












