# Introduction

 The document holds the time sheets for my work during 2022 Summer Internship at VLEAD, IIIT Hyderabad.

# Work Logs

## Week 1 [2022-05-16 Mon] till [2022-05-20 Fri]

## Daily Logs

- Day1 [2022-05-16 Mon]: Introductory Meet assigned projects to work on , and discussed about the internship plan as we go through.
- Day2 [2022-05-17 Tue] : Looked at the concepts of counters (the first experiment I was going to work on)
- Day3 [2022-05-18 Wed] : Going through the codebases of the earlier experiments. 
- Day4 [2022-05-19 Thu] : Going through the codebase making a plan for the experiment
- Day5 [2022-05-20 Fri] : Started coding with practice section of the experiment was done making the task 1 of practice section i.e basic counters.

## Week 2 [2022-05-23 Mon] till [2022-05-27 Fri]

## Daily Logs

- Day1 [2022-05-23 Mon]: Started with task 2 i.e. ring counter of practice section made the necessary components that were needed in this experiment.
- Day2 [2022-05-24 Tue] : Completed task 2 of practice section
- Day3 [2022-05-25 Wed] : Going through the GSAP library and creating svgs to understand simulations and animations. Made the animation/ demo section for the ring counter.
- Day4 [2022-05-26 Thu] : Fixed bugs and handling corner cases that user may face if they made a wrong circuit.
- Day5 [2022-05-27 Fri] : Showed the progress to the mentors, removed alert boxes and showed descriptive errors. Removed bugs

## Week 3 [2022-05-30 Mon] till [2022-06-03 Fri]

## Daily Logs

+ Day1 [2022-05-30 Mon]: Finishing touches to the experiment, corrected experiment descriptor, edited procedure.md and created highlight features for the incorrectly connected components to the user.
+ Day2 [2022-05-31 Tue] : added and changed theory , procedure and instructions for the experiment ,  made suggested changed according to the code review - Code Refactoring
+ Day3 [2022-06-01 Wed] : Created pretest.json and posttest.json and corrected the experiment descriptor.json
+ Day4 [2022-06-02 Thu] : Changed the whole structure of animation part, reduced global variables, pushed the code to testing branch.
+ Day5 [2022-06-03 Fri] : Checked the UI and the files after pushing the changes to testing

## Week 4 [2022-06-06 Mon] till [2022-06-10 Fri]

## Daily Logs

+ Day1 [2022-06-06 Mon]: Started working of second track and revised the theory for the next experiment state diagram
+ Day2 [2022-06-07 Tue] : Made the animation for the state diagram experiment, added the state diagram in the in the observation table, after the simulation user is also able to see the state table
+ Day3 [2022-06-08 Wed] : Refactoring the code for the first experiment, improving UI.
+ Day4 [2022-06-09 Thu] : completed the animation part, started with practice section, edited the practice.html and layout.js. Changed the asynchronous nature of JK flip-flops to synchronous.
+ Day5 [2022-06-10 Fri] : Gone through the code of the repository ph3-lab-mgmt. Made a plan for the second track

## Week 5 [2022-06-13 Mon] till [2022-06-17 Fri]

## Daily Logs

+ Day1 [2022-06-13 Mon]: Completed the validation and simulation part of the experiment state diagram,created posttest, pretest for state diagrams experiment along with code refactoring, resolved issues with testing Google analytics on sample html page.
+ Day2 [2022-06-14 Tue]: Code refactoring
+ Day3 [2022-06-15 Wed]: Started demo section for multipliers 
+ Day4 [2022-06-16 Thu]: Completed demo section for multipliters and started demo section of ALU
+ Day5 [2022-06-17 Fri]: Completed demo section of ALU 

## Week 6 [2022-06-20 Mon] till [2022-06-24 Fri]

## Daily Logs

+ Day1 [2022-06-20 Mon]: Completed practice section of multipliers
+ Day2 [2022-06-21 Tue]: Completed practice section of ALU
+ Day3 [2022-06-22 Wed]: Fixed bugs in multipliers demo
+ Day4 [2022-06-23 Thu]: Fixed bugs in ALU practice section
+ Day5 [2022-06-24 Fri]: Fixed bugs in ALU demo section started implementing GA4.

## Week 7 [2022-06-27 Mon] till [2022-07-01 Fri]

## Daily Logs

+ Day1 [2022-06-27 Mon]: Fixed bugs in practice section of ALU, resolved issues regarding permissions
+ Day2 [2022-06-28 Tue]: Code refactoring, got to know about containers being used and triggers/tags
+ Day3 [2022-06-29 Wed]: Code refactoring of counters experiments 
+ Day4 [2022-06-30 Thu]: Fixed bugs in counters experiments
+ Day5 [2022-07-01 Fri]: Made eslintrc and added eslint configuration in ph3-lab-mgmt

## Week 8 [2022-07-04 Mon] till [2022-07-08 Fri]

## Daily Logs

+ Day1 [2022-07-04 Mon]: Started working on the demo section of the experiment https://github.com/virtual-labs/exp-transistor-level-inverter-iiith. 
+ Day2 [2022-07-05 Tue]: Finished the demo section of the above experiment.
+ Day3 [2022-07-06 Wed]: Refactor all the demos of the 2 DLD experiment.
+ Day4 [2022-07-07 Thu]: Refactor the demos of next 2 DLD experiment.
+ Day5 [2022-07-08 Fri]: Finished the second track. The main code is in the repo https://github.com/virtual-labs/build-validation. I have created a new pluginscope by the name POSTBUILD, added a new plugin function processPostBuildPlugins in plugin.js and this function is called after experiment build in experiment.js.

## Week 9 [2022-07-11 Mon] till [2022-07-15 Fri]

## Daily Logs

+ Day1 [2022-07-11 Mon]: Completed the demo section of the experiment `exp-transistor-level-nand-iiith`, there were 2 subtasks for the demo in this experiment that are nand and nor logic. For these 2 parts, the wires will intersect at some places as a result we have used wires of 2 different colours to distinguish between the wires.
+ Day2 [2022-07-12 Tue]: Completed the demo section of the experiment `exp-pass-transistor-logic-iiith`. There were 2 subtasks for the demo in this experiment that are pass transistor and multiplexer. 
+ Day3 [2022-07-13 Wed]: Completed the demo section of the experiment
  `exp-transistor-level-xor-iiith` . There were 2 subtasks of XOR and XNOR logic.
+ Day4 [2022-07-14 Thu]: Today's work update: Completed the demo section of `exp-d-latch-and-d-flip-flop-iiith` . There were 2 subtasks: Latch and FlipFlop.
+ Day5 [2022-07-15 Fri]: Refactored the above demo sections.

## Week 10 [2022-07-18 Mon] till [2022-07-22 Fri]

## Daily Logs

+ Day1 [2022-07-18 Mon]: Made changes to the practice section of the first VLSI experiment and implemented the different colors of wires.
+ Day2 [2022-07-19 Tue]: Resolved the issues in the first experiment and made similar changes in the following 2 experiments i.e https://github.com/virtual-labs/exp-transistor-level-xor-iiith and https://github.com/virtual-labs/exp-transistor-level-nand-iiith.
+ Day3 [2022-07-20 Wed]: Made demo and practice section of chain of inverters
+ Day4 [2022-07-21 Thu]: Updated the practice section of remaining two VLSI experiment updated instruction box and experiment-descriptor.json.
+ Day5 [2022-07-22 Fri]: Made mobile version of the VLSI experiments.

# Week Progress

| S.No |              Overall Experiment Status               |               Previous Week Milestone                |                 Next Week Milestone                  |                            Issues                            |
| :--: | :--------------------------------------------------: | :--------------------------------------------------: | :--------------------------------------------------: | :----------------------------------------------------------: |
|  1.  |              Started with experiment 7               |                          -                           |                 Finish experiment 7                  | Learn about the new libraries and understanding the previous codebase to create a new experiment from scratch |
|  2.  |                Experiment 7 completed                |           Started coding the experiment 7            | 1. Finishing practice section of the next experiment | Displaying descriptive error messages in experiment 7. According to suggestions have to make changes so that the user have a smooth and enriched experience. |
|  3.  |               Refactored Experiment 7                |                Experiment 7 completed                |   1. Finishing demo section of the next experiment   |        Code refactored with all the suggested changes        |
|  4.  | Demo section of Experiment 10. Starting Second track |               Refactored Experiment 7                |   1. Finish Experiment 10 and the next experiment    | Gone through the code base for second track, made a plan for that. Finished the demo section as well as simulation part of the practice section of the state diagram experiment |
|  5.  |                 Completed Multiplier                 | Demo section of Experiment 10. Starting Second track |                    Completed ALU                     |                           No Issue                           |
|  6.  |                    Completed ALU                     |                 Completed Multiplier                 |                    Code refactor                     |                           No Issue                           |
|  7.  |                    Code refactor                     |                    Completed ALU                     |           Animations of 4 VLSI experiment            |                           No Issue                           |
|  8.  |           Animations of 4 VLSI experiment            |                    Code refactor                     |     Animation of 2 VLSI and changed 2 experiment     |                           No Issue                           |
|  9.  |     Animation of 2 VLSI and changed 2 experiment     |           Animations of 4 VLSI experiment            |      Changed 4 VLSI experiment made mobile view      |                           No Issue                           |
| 10.  |      Changed 4 VLSI experiment made mobile view      |     Animation of 2 VLSI and changed 2 experiment     |                          -                           |                           No Issue                           |
