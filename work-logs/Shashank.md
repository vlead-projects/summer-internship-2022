# Introduction

  This document holds the time sheets for my work during 2022 Summer Internship at VLEAD, IIIT Hyderabad.

# Work Logs

## Week 1 [2022-05-30 Mon] till [2022-06-03 Fri]

## Daily Logs

- Day1 [2022-05-30 Mon]: Had our first meeting with the internship heads and gothttps://gitlab.com/vlead-projects/summer-internship-2022.git our tasks of the critical review of experiments.

- Day2 [2021-05-31 Tue] : Completed the critical review for the mergesort lab while working on java script fundamentals(Section 1 3 - 6)

- Day3 [2021-06-01 Wed]:  Worked on java script fundamentals, started working on the critical review for linked lists(Section 1 6-10)

- Day4 [2021-06-02 Thur]: Completed critical review for linked lists, continued java script fundamentals(Section 1 11-14)

- Day5 [2021-06-03 Fri]:  Java Script continuation (Section 1-3 ) Tried out certain code from the exercises given in the website. Went through the merge sort and linked-list code to test my understanding.

## Week 2 [2022-06-6 Mon] till [2022-06-10 Fri]

## Daily Logs

- Day1 [2022-05-30 Mon]: Had our  meeting with the internship team and gothttps://gitlab.com/vlead-projects/summer-internship-2022.git our tasks to fix  bugs 

- Day2 [2021-05-31 Tue] : Fixed bug - https://github.com/virtual-labs/exp-stacks-queues-iiith/issues/15 and worked on  HTML and CSS 

- Day3 [2021-06-01 Wed]:  Fixed Bug - https://github.com/virtual-labs/exp-stacks-queues-iiith/issues/17 and fixed bug -  https://github.com/virtual-labs/exp-stacks-queues-iiith/pull/18

- Day4 [2021-06-02 Thur]: Started implementation for the slider in the heap-sort lab and fixed bug in the heapsort lab - https://github.com/virtual-labs/exp-heap-sort-iiith/issues/9

- Day5 [2021-06-03 Fri]:  Continued HTML and CSS basics and fixed bug - https://github.com/virtual-labs/exp-heap-sort-iiith/pull/10

## Week 3 [2022-06-13 Mon] till [2022-06-17 Fri]

## Daily Logs

- Day1 [2022-05-30 Mon]: Had  meeting with the internship team and gothttps://gitlab.com/vlead-projects/summer-internship-2022.git our tasks of starting working in depth into CI/CD and advanced bugs ans features

- Day2 [2021-05-31 Tue] : Fixed bugs - https://github.com/virtual-labs/exp-tree-traversal-iiith/issues/12 and https://github.com/virtual-labs/exp-tree-traversal-iiith/issues/14

- Day3 [2021-06-01 Wed]: Worked on implementing an undo feature in the tree-traversal exercise while making the whole code much better written. 

- Day4 [2021-06-02 Thur]: Implemented the undo feature and made the code much better written

- Day5 [2021-06-03 Fri]: Fixed changes requested for my issues and added another feature that makes the undo button grey when clicked 

## Week 4 [2022-06-20 Mon] till [2022-06-25 Fri]

## Daily Logs

- Day1 [2022-05-30 Mon]: Had  meeting with the internship team and gothttps://gitlab.com/vlead-projects/summer-internship-2022.git our tasks of starting working in depth into modularization.

- Day2 [2021-05-31 Tue] :Continued implementing the undo feature and fixing advanced bugs. 

- Day3 [2021-06-01 Wed]: Worked on the modularization of the code of the tree-traversal

- Day4 [2021-06-02 Thur]: Met with other interns for assistance and succesfully modularized the code for Depth-first-traversal

- Day5 [2021-06-03 Fri]: Successfully modularized the whole tree-traversal code and implemented the tree slider in heap-sort.

## Week 5 [2022-06-27 Mon] till [2022-07-1 Fri]

## Daily Logs

- Day1 [2022-06-27 Mon]: Had  meeting with the internship team and gothttps://gitlab.com/vlead-projects/summer-internship-2022.git our tasks of starting working in depth into my own lab.

- Day2 [2021-06-28 Tue] : Started implementing the demo for insertion sort

- Day3 [2021-06-29 Wed]: Continued implementing the demo for the insertion sort 

- Day4 [2021-06-30 Thur]: Met with other interns for assistance and succesfully implemented the visual aspects

- Day5 [2021-07-01 Fri]: Complemeted the demo's first draft


## Week 6 [2022-07-4 Mon] till [2022-07-8 Fri]

## Daily Logs

- Day1 [2022-07-04 Mon]: Had  meeting with the internship team and gothttps://gitlab.com/vlead-projects/summer-internship-2022.git our tasks of starting working in depth into my own lab.

- Day2 [2021-07-05 Tue] : Started implementing the exercise for insertion sort

- Day3 [2021-07-06 Wed]: Continued implementing the exercise for the insertion sort 

- Day4 [2021-07-07 Thur]: Met with other interns for assistance and succesfully implemented the visual aspects

- Day5 [2021-07-08 Fri]: Complemeted the exercise's first draft


## Week 7 [2022-07-11 Mon] till [2022-07-15 Fri]

## Daily Logs

- Day1 [2022-07-04 Mon]: Had  meeting with the internship team and gothttps://gitlab.com/vlead-projects/summer-internship-2022.git our tasks of starting working in depth into my own lab.

- Day2 [2021-07-05 Tue] : Started implementing the exercise for insertion sort

- Day3 [2021-07-06 Wed]: Continued implementing the exercise for the insertion sort 

- Day4 [2021-07-07 Thur]: Met with other interns for assistance and succesfully implemented the visual aspects

- Day5 [2021-07-08 Fri]: Complemeted the exercise's first draft


## Week 8 [2022-07-18 Mon] till [2022-07-22 Fri]

## Daily Logs

- Day1 [2022-07-11 Mon]: Had  meeting with the internship team and gothttps://gitlab.com/vlead-projects/summer-internship-2022.git our tasks of starting working in depth into my own lab.

- Day2 [2021-07-12 Tue] : Started implementing the second draft for demo , exercise and demo

- Day3 [2021-07-13 Wed]: Continued implementing the second draft for demo , exercise and demo

- Day4 [2021-07-14 Thur]: Met with other interns for assistance and succesfully implemented the visual aspects

- Day5 [2021-07-15 Fri]: Complemeted the second drafts


## Week 9 [2022-07-25 Mon] till [2022-07-29 Fri]

## Daily Logs

- Day1 [2022-07-11 Mon]: Had  meeting with the internship team and gothttps://gitlab.com/vlead-projects/summer-internship-2022.git our tasks of starting working in depth into my own lab.

- Day2 [2021-07-12 Tue] : Started implementing the second draft for demo , exercise and demo

- Day3 [2021-07-13 Wed]: Continued implementing the second draft for demo , exercise and demo

- Day4 [2021-07-14 Thur]: Met with other interns for assistance and succesfully implemented the visual aspects

- Day5 [2021-07-15 Fri]: Complemeted the second drafts



## Week 10 [2022-07-1 Mon] till [2022-07-5 Fri]

## Daily Logs

- Day1 [2022-07-11 Mon]: Had  meeting with the internship team and gothttps://gitlab.com/vlead-projects/summer-internship-2022.git our tasks of starting working in depth into my own lab.

- Day2 [2021-07-26 Tue] : Started fixing bugs on demo

- Day3 [2021-07-27 Wed]:  Started fixing bugs on practice

- Day4 [2021-07-28 Thur]: Met with other interns for assistance and succesfully implemented the visual aspects

- Day5 [2021-07-29 Fri]:  Started fixing bugs on exercise


 










 
# Week Progress

| S.No | Overall Experiment Status | Previous Week Milestone | Next Week Milestone | Issues|
| :---: | :---: | :---: |:---: |:---: |
| 1. | Finished critical reviews  | Had to finish the critical reviews | Get into the coding part of the experiments |Need to learn more about java-script , HTML and CSS |
| 2. | Many bugs fixed in Stacks and Queues and labs | Start fixing bugs and get involved in HTML and CSS  | Start implementing advanced features and fixing complicated bugs  |Need to learn more about java-script , HTML and CSS, work more on the dev console  |
| 3. | Tree-traversal exercises remodeled completely and undo feature added| Implement features and fixed advanced bugs  |Start making the tree-traversal modular |Work on building on my own  experiment  |
| 4. | Tree-traversal exercises remodeled completely and modularized,heap-sort slider implemented| Modularize code  |Implementing library |Work on building my own experiment
| 5. |Insertion sort demo| Modularize code  |Insertion sort practice |Fix visual aspects
| 6. |Insertion sort practice| Insertion sort demo  |Insertion sort exercise |Fix visual aspects
| 7. |Insertion sort exercise| Inserrtion sort practice  |Implement anime.js |Fix visual aspects
| 8. |Implementing anime.js into the code| Insertion sort exercise  |Ditch anime.js and and arrows |Fix visual aspects
| 9. |Added arrows| Tried using async functions with anime.js   |Wrap up internship |Fix visual aspects
| 10. |Wrapped up internship | Tried using async functions with anime.js   |Wrap up internship |Finish work logs 









 












