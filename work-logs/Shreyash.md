
# Introduction
 This document holds the time sheets for my work during 2022 Summer Internship at VLEAD, IIIT Hyderabad.
# Work Logs
## Week 1 [2022-05-16 Mon] till [2022-05-20 Fri]
## Daily Logs
- Day1 [2022-05-16 Mon]: Had introductory meeting. Held a meeting with mentees.
- Day2 [2022-05-17 Tue] : Went through the code for Digital Logic Design lab, explained how the code works to other interns.
- Day3 [2022-05-18 Wed] : Collaborated in writing the code and fixing bugs.
- Day4 [2022-05-19 Thu] : Had meeting to check progress and clarify doubts.
- Day5 [2022-05-20 Fri] : Reviewed experiment 1 of Digital Logic Design Lab.
## Week 2 [2022-05-23 Mon] till [2022-05-27 Fri]
## Daily Logs
- Day1 [2022-05-23 Mon]: Fixed some code smells in the code for experiment 1 in DLD lab.
- Day2 [2022-05-24 Tue] : Had meeting for discussion of Computer Graphics lab with other intern.
- Day3 [2022-05-25 Wed] : Reviewed experiment 2 of Digital Logic Design Lab. Made necessary changes.
- Day4 [2022-05-26 Thu] : Had weekly status update meeting.
- Day5 [2022-05-27 Fri] : Meeting with mentees over their progress, cleared doubts, suggested ideas.
## Week 3 [2022-05-30 Mon] till [2022-06-03 Fri]
## Daily Logs
- Day1 [2022-05-30 Mon]: Code Review for experiment 10 in CG lab.
- Day2 [2022-05-31 Tue] : Code Review for experiment Counters in DLD lab.
- Day3 [2022-06-01 Wed] : Had a meeting to explain posttest,pretest and experiment descriptor. Asked mentees to finalize experiment and submit for review again.
- Day4 [2022-06-02 Thu] : Weekly status update.
- Day5 [2022-06-03 Fri] : Reviewed experiment 3 of Digital Logic Design Lab. Reviewed codes of intern and suggested changes again.
## Week 4 [2022-06-06 Mon] till [2022-06-10 Fri]
## Daily Logs
- Day1 [2022-06-06 Mon]: Had a meeting with DLD team to plan the next experiment, state diagrams. Counters in DLD lab sent for final code review.
- Day2 [2022-06-07 Tue] : Planning alternatives and researching for different ways of implementation in state diagrams.
- Day3 [2022-06-08 Wed] : Discussion with interns over their upcoming plans and how they plan to finish all the assigned tasks.
- Day4 [2022-06-09 Thu] : Weekly status update.
- Day5 [2022-06-10 Fri] : Reviewed experiment 4 of Digital Logic Design Lab. Reviewed first sample of state diagrams experiment. Suggested changes.
## Week 5 [2022-06-13 Mon] till [2022-06-17 Fri]
## Daily Logs
- Day1 [2022-06-13 Mon]: Rewrote animation files for Adder Circuit.
- Day2 [2022-06-14 Tue] : Improved HTML files for demo part.
- Day3 [2022-06-15 Wed] : Code discussion and debugging with interns.
- Day4 [2022-06-16 Thu] : Weekly status update.
- Day5 [2022-06-17 Fri] : Gave more changes in State Diagrams after review.

## Week 6 [2022-06-20 Mon] till [2022-06-24 Fri]
## Daily Logs
- Day1 [2022-06-20 Mon]: Rewrote animation files for Multiplexer Experiment.
- Day2 [2022-06-21 Tue] : Fixed layout bugs in Adder Circuit and multiplexer.
- Day3 [2022-06-22 Wed] : Changed instruction boxes in 3 DLD experiments.
- Day4 [2022-06-23 Thu] : Changed instruction boxes in next 3 DLD experiments.
- Day5 [2022-06-24 Fri] : Approved state diagrams and merged it to testing.

## Week 7 [2022-06-27 Mon] till [2022-07-01 Fri]
## Daily Logs
- Day1 [2022-06-27 Mon]: Rewrote animation files for Comparator Experiment.
- Day2 [2022-06-28 Tue] : Fixed HTML and layout.js files
- Day3 [2022-06-29 Wed] : Reviewed Multipliers experiment, suggested changes.
- Day4 [2022-06-30 Thu] : Weekly status update.
- Day5 [2022-07-01 Fri] : Code debugging and discussion with interns.

## Week 8 [2022-07-04 Mon] till [2022-07-08 Fri]
## Daily Logs
- Day1 [2022-07-04 Mon]: Rewrote animation files for 7-Segment Display Experiment.
- Day2 [2022-07-05 Tue] : Fixed HTML and layout.js files
- Day3 [2022-07-06 Wed] : Meeting with other interns for code debugging.
- Day4 [2022-07-07 Thu] : Weekly status update.
- Day5 [2022-07-08 Fri] : Reviewed ALU experiment and suggested changes.

## Week 9 [2022-07-11 Mon] till [2022-07-15 Fri]
## Daily Logs
- Day1 [2022-07-11 Mon]: Rewrote animation files for Flip Flops Experiment.
- Day2 [2022-07-12 Tue] : Made changes to previous experiments after code review.
- Day3 [2022-07-13 Wed] : Made changes to previous experiments after code review.
- Day4 [2022-07-14 Thu] : Weekly status update.
- Day5 [2022-07-15 Fri] : Approved changes in multipliers and ALU, merged them to testing.

## Week 10 [2022-07-18 Mon] till [2022-07-22 Fri]
## Daily Logs
- Day1 [2022-07-18 Mon]: Rewrote animation files for Register Experiment.
- Day2 [2022-07-19 Tue] : Fixed layout files and several bugs in the experiment.
- Day3 [2022-07-20 Wed] : Made changes to Register Experiment after code review.
- Day4 [2022-07-21 Thu] : Pushed all changes for DLD finally.
- Day5 [2022-07-22 Fri] : Final status update meeting of the summer internship. Finished all code reviews

# Week Progress
| S.No | Overall Experiment Status | Previous Week Milestone | Next Week Milestone | Issues|
| :---: | :---: | :---: |:---: |:---: |
| 1. |Began interacting with interns and reviewing old code for DLD lab | | Review Experiment 2 of DLD lab | No issues faced yet.|
| 2. |Suggesting ideas, fixing bugs and reviewing code | Review Experiment 2 of DLD lab| Expect code review #1 for DLD and CG lab. Fix old code of DLD lab (animation part)| Waiting for pull requests for code review.
| 3. |Code Reviews for Interns | Expect code review #1 for DLD and CG lab. Fix old code of DLD lab (animation part)| Approve codes for final review by Raj. Fix animation part of DLD lab.  | Waiting for final changes to approve code review #1.
| 4. |Code Reviews and ideation for state diagrams | Approve codes for final review by Raj. Fix animation part of DLD lab.| Push State diagrams experiment for final code review | No issues.
| 5. |Fixed Adder Circuit, reviewed State Diagrams | Push State diagrams experiment for final code review| Push State diagrams experiment for final code review | No issues.
| 6. |Fixed Multiplexer, merged State Diagrams | Push State diagrams experiment for final code review| Push Multipliers experiment for final code review | No issues.
| 7. |Fixed Comparator, reviewed Multipliers | Push Multipliers experiment for final code review| Push Multipliers experiment for final code review | No issues.
| 8. |Fixed 7-segment Display, reviewed ALU | Push Multipliers experiment for final code review| Push Alu and Multipliers experiment for final code review | No issues.
| 9. |Fixed Flip-Flops, pushed ALU and Multipliers | Push Alu and Multipliers experiment for final code review| Finalize registers and push all changes to DLD | No issues.
| 10. |Finished DLD completely | Finalize registers and push all changes to DLD|  | No issues.
