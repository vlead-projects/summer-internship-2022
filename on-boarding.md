# Welcome to Virtual Labs - 2022 Summer Internship Program @ IIITH!

  We are delighted that you are joining us as a new intern. This program has been developed for students to gain practical training and hands-on, real world experience that will help them meet their educational goals and prepare them for a professional career.  The VLEAD team is here to support your transition, so you can call on any of us to assist you.

## Instructions to Interns

### Working Hours

Your working hours for the internship period is from 9:00 A.M to 6:00P.M (Monday - Friday).

### Leave Policy

You are not allowed to take any leave during the internship period. 

### Work Logs

1. Follow the [instructions.md](./work-logs/instructions.md) to create a work-logs directory.

2. You have to get your work-log approved from your mentor on a weekly basis. You will need to submit your approved work-log to ravi@vlabs.ac.in. Your stipend will be released against this submission only.
      
### Presentations

1. Tuesday/Thursday mornings will be reserved for a short presentation (30 min) by the interns on their project and progress.

2. A final presentation (with links to relevant documents) will need to be made during the last week of the internship against which the internship certificate will be issued.

### Slack Channel

Communication is made through [Slack](https://vleadworkspace.slack.com) during the internship period. If you need any guidance regarding anything, just post it on [2022-summer-interns](https://vleadworkspace.slack.com/archives/C012G7UKTC3) channel. To get access to this channel send out a mail to *pavan@vlabs.ac.in*.  Please be available at all times on [2022-summer-interns](https://vleadworkspace.slack.com/archives/C012G7UKTC3) channel on [Slack](https://vleadworkspace.slack.com/messages).
