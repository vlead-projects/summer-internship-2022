# Virtual Labs - Summer Internship 2022 at IIITH

This repository captures the list of the documents for 2022 summer interns.

| S.No | Document Name | Document Link |
| :---: | :---: | :---: |
| 1. | On Boarding |[Link](./on-boarding.md) |
| 2. | Resources | [Link](./resources.md) |
| 2. | Project Assignments | [Link](./project-assignments.md)|
| 3. | Worklogs | [Link](./work-logs/index.md)|













