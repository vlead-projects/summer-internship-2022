# Introduction

This document captures the resource links for 2022 summer interns.

# Beginner Level 

## Version Control:

- Virtual Labs uses git for version control. Most of the source code lies in different repositories on GitLab and GitHub. 

- [Git-scm](https://git-scm.com/docs/gittutorial) is a good resource for all levels.

## HTML5 and CSS3:

- [w3schools](https://www.w3schools.com/) has good tutorials on all web technologies. Both the [HTML](https://www.w3schools.com/html/) and [CSS](https://www.w3schools.com/css/default.asp) tutorials can be accessed there.

- [w3org CSS](https://www.w3.org/Style/Examples/011/firstcss) Tutorial is also a good place to start learning CSS.

## Javascript:

- [Javascript.info](https://javascript.info/) has a very good set of interactive Javascript learning resources and is available for free.

- [Javascript30](https://javascript30.com/) has a list of 30 projects, each accompanied by a video. The projects can provide good practice after learning the basics of Javascript and DOM.

- [LearnJSOnline](https://learnjavascript.online/) : Good resources, paid [MDN](https://developer.mozilla.org/en-US/docs/Learn/Getting_started_with_the_web) has many good resources for new web developers.

## Development Environment options:

   1. Online options like [CodePen](https://codepen.io/), [JSfiddle](https://jsfiddle.net/), [codesandbox](https://codesandbox.io/), [html-css-js](https://html-css-js.com/)
   2. Local with an IDE like VS Code and a server like live-server or python http module.

# Intermediate Level 

## Dev Resources 

1. [MDN](https://developer.mozilla.org/en-US/)
2. [Google JS Style Guide](https://google.github.io/styleguide/jsguide.html)
3. [WebPageTest](https://webpagetest.org/)
4. [Modern CSS Solutions](https://moderncss.dev/)
5. [Inkscape](https://inkscape.org/)
6. [jsPlumb](https://docs.jsplumbtoolkit.com/community/)

## Virtual Labs Resources

1. [Virtual Style CSS](https://github.com/virtual-labs/virtual-style)
2. [Exp Development Process](https://github.com/virtual-labs/ph3-exp-dev-process/tree/main/dev-process)
3. [Coding Best Practices](https://github.com/virtual-labs/ph3-exp-dev-process/tree/main/best-practices)
4. Sample Code: Virtual Labs experiment source code base can be found on the [Virtual Labs organization on GitHub](https://github.com/virtual-labs). In particular, you can take a look at the [Added Circuit](https://virtual-labs.github.io/exp-adder-circuit-iiith/) Experiment deployment and the [testing branch in the repo](https://github.com/virtual-labs/exp-adder-circuit-iiith/tree/testing).


